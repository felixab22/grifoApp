import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm:FormGroup;

  constructor(
    private auth:AngularFireAuth, 
    private fb:FormBuilder,
    private _router: Router
  ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email:["", Validators.required],
      password:["", Validators.required]
    })
  }
  login(){
    this.auth.signInWithEmailAndPassword(
      this.loginForm.controls['email'].value,
      this.loginForm.controls['password'].value
    ).then((userData:any)=>{
      console.log(userData);
      localStorage.setItem("uid", userData.user.uid)
      this._router.navigate(['/tabs/tab1'], {replaceUrl:true})
    })
    console.log(this.loginForm.value);
  }
}
