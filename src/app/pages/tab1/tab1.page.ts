import { Component } from '@angular/core';
import { SurtidoresService } from '../../services/surtidores.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  list851: any;
  surtidores = [
    {id: 1, titulo: 'SURTIDOR 1', tipo: 'G85', icon:'archive-outline'},
    {id: 2, titulo: 'SURTIDOR 2', tipo: 'G85', icon:'beaker-outline'},
    {id: 3, titulo: 'SURTIDOR 1', tipo: 'G90', icon:'color-fill-outline'},
    {id: 4, titulo: 'SURTIDOR 2', tipo: 'G90', icon:'leaf-outline'},
    {id: 5, titulo: 'SURTIDOR 1', tipo: 'G95', icon:'pint-outline'},
  ]

  constructor(
    private _SurtidorSrv: SurtidoresService
  ) {
    // console.log(this._SurtidorSrv.items);
    
  }
  ngOnInit(): void {
   
    
  }
  registrar(item){
    console.log(item);
    
  }
  listar851(){
   this.list851 = this._SurtidorSrv.exportarSurtidor851();
   console.log(this.list851);
   
  }
}
